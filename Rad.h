/** @file
 * @ingroup group_rapid_amplitude_data
 * @brief Header file for Rad.C
 */
/***********************************************************

File Name :
        Rad.h

Original Author:
        Paul Friberg

Description:


Creation Date:
        5 May 2005

Modification History:
	21 Dec 2007 - updated to support leap seconds and new tntime classes


Usage Notes:


**********************************************************/

#ifndef rad_H
#define rad_H

#include <iostream>
#include <vector>
#include <cstdio>
#include <cstring>
#include <cerrno>
#include <cstdlib>

#include "RetCodes.h"
#include "seismic.h"
#include "gcda.h"
#include "ddcr.h"
#include "dwriter.h"
#include "dreader.h"
#include "dchan.h"
#include "chan.h"
#include "findchancfg.h"
#include "AmplitudeADA.h"
#include "cvert.h"
#include "chancfg.h"
#include "nscl.h"
#include "ewLogA0.h"


#include "Application.h"
#include "LockFile.h"
#include "ewLogA0.h"


class Rad : public Application 
{
 private:
    int seconds_in_exam_window;
    float q_b_factor;
    float q_s_factor;
    float q_p_factor;
    float minimum_acceleration;
    float maximum_velocity;
    float lta_window_bb;
    float lta_window_sm;
    char lock_file_name[MAXSTR];
    LockFile *lock;

    ewLogA0  *ew;	// used for ew LogA0 files
    float ml100_correction;	// computed from ew, or -9.0 for not used


    char wda_key_name[GCDA_KEY_NAME_LEN];
    char ada_key_name[GCDA_KEY_NAME_LEN];

    char progname[MAXSTR];    // used for RAD lookup in program/config_cannel tables
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;

    int number_of_channels_found;

    float was_gain;

    struct channel_identifier_type channels[MAX_CHANNELS_IN_NETWORK];

    Generic_CDA<int>*wda;
    Generic_CDA<struct amplitude_data_type> *ada;
    Defined_Data_Channel_Reader<int> *wave_reader;
    Data_Channel_Writer<struct amplitude_data_type> *amp_writer;
    std::vector<Convert_Waves_to_Amplitudes> cvert_list;

    int _InitByFiles();
    int _InitByDB();

 public:

    Rad();
    ~Rad();
    int ParseConfiguration(const char *tag, const char *value);
    int Startup(); 
    int Run();
    int Shutdown(); 
};
#endif
