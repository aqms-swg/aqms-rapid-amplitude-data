/** @file
 * @ingroup group_rapid_amplitude_data
 * @brief Header file for c1c2s.f
 */
/***********************************************************

File Name :
 

Programmer:
	Phil Maechling

Description:
	This is a modification of c1c2s.f. It calculates the
	magnitude parameters once, and then returns them.
	They are then passed into rmag31 and use for magnitude
	calculations.

Creation Date:
	30 June 1998

Usage Notes:



Modification History:



**********************************************************/

#ifndef c1c2s_H
#define c1c2s_H

#ifdef __cplusplus
extern "C" {
#endif

    void c1c2s_(int* icha,
		float* dt,
		int* iout,
		float* f0,
		float* h,
		float* g,
		float* c1,
		float* c2,
		float* hcrit,
		float* ramult,
		float* gwas);
#ifdef __cplusplus
}

#endif
#endif
