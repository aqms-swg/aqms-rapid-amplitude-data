/** @file
 * @ingroup group_rapid_amplitude_data
 * @brief Header file for DataBuffer.C
 */
/***********************************************************

File Name :
	DataBuffer.h	

Programmer:
	Pete Lombard

Description:
	A buffer of time-series data


Creation Date:
	6 December 2005


Usage Notes:



Modification History:
	21 Dec 2007 - updated to support leap seconds and new tntime classes



**********************************************************/
#ifndef DataBuffer_H
#define DataBuffer_H

#include <vector>
#include "Duration.h"
#include "TimeStamp.h"

typedef std::vector< int > IntVector;

class DataBuffer 
{
    
 public:

    DataBuffer();
    DataBuffer(int *data, int nsamples, TimeStamp &starttime, double interval);
    ~DataBuffer();

    void startData(int *data, int nsamples, TimeStamp &starttime, double interval);
    int appendData(int *waveform, int nsamples);
    int setPosTime(TimeStamp &time);
    TimeStamp getPosTime();
    int getData(int *waveform, int max_samples, TimeStamp &time);
    bool isStarted();
    bool empty();
    int size();
    void clear();
    
 private:

    IntVector data;
    int pos;
    TimeStamp startTime;
    double sampleInterval;
    bool started;
};

#endif
