C> @file
C> @ingroup group_rapid_amplitude_data
C> @brief Program to compute magnitude etc real time
c/***********************************************************
c
cFile Name :
c     rmag31.f
c
cProgrammer:
c	Phil Maechling
c
cDescription:
c 	This a modification of realmag2.f, hence realmag3.f
c       This is a single component version, hence realmag31.f
c	This is for a single component of data.
c	The samples are passed in as an array.
c
cCreation Date:
c	5 August 1996
c
c Usage Notes:
c
c 20sps    broad band     = 0 
c 80-100sps strong motion = 1
c 80-100sps broadband     = 2
c
c
c
c
c Modification History:
c 24 March 1997
c   Added return parameter of uncorrected wa amp
c
c  modified from real0.f
c  Aug.29, 1995  hiroo kanamori
c  program to compute magnitude etc real time
c  real-time mode using recursive filter
c  For vbb channel with dt=0.05 sec, a modified set of W-A constants
c  is used to correct for the error caused by the use of
c  computes time-stamped ML and ME computed for each tw-window and a default
c  distance of 100 km
c
c --  all the amplitude values are in c.g.s.   energy is in ergs
c
c 2007/04/30, PNL: use CISN logAo function to compute ml100
c 2008/06/30, paulf: added capability to use other logA0 func via ew code
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
    	
       subroutine realmag31(ibegin,
     2 nsamples,dt,q,itype,
     2 agwa,ac1wa,ac2wa,
     2 agsp03,ac1sp03,ac2sp03,
     2 agsp10,ac1sp10,ac2sp10,
     2 agsp30,ac1sp30,ac2sp30,
     2 aamult,
     2 gf,amlcor,amecor,
     2 rzx,rzya,rzyv,rzyd,rzywa,rzyen,
     2 rsp03,rsp10,rsp30,
     2 amaxo,amaxq,iamax,
     2 vmaxo,vmaxq,ivmax,
     2 dmaxo,dmaxq,idmax,
     2 sp03maxo,sp03maxq,isp03max,
     2 sp10maxo,sp10maxq,isp10max,
     2 sp30maxo,sp30maxq,isp30max,
     2 uwamaxo,cwamaxo,wamaxq,iwamax,
     2 enmaxo,
     2 renergy,
     2 rml100,
     2 rme100,
     2 rml100corr,
     2 ismpls)
c
c ---  type=channel b = 0 or s = 1 or p = 2
c ---   gf(i) are the instrument response 
c ---   nsamples is the number of nsamples passed in to routine
c
c
c Define the calling parameters
c
      implicit none
      include 'samples.inc'

c
c Input Parameters
c
      integer ibegin
      integer nsamples
      integer nw
      integer itype

      real    gf
      real    dt,q

      real agwa,ac1wa,ac2wa
      real agsp03,ac1sp03,ac2sp03
      real agsp10,ac1sp10,ac2sp10
      real agsp30,ac1sp30,ac2sp30

      real amlcor,amecor
      real aamult
c
c End of input parameters
c
c accel,vel,disp,wa-amp and a quality value for each
c also a intergral of velocity squared value enmaxo
c
      real    amaxo,amaxq
      real    vmaxo,vmaxq
      real    dmaxo,dmaxq
      real    sp03maxo,sp03maxq
      real    sp10maxo,sp10maxq
      real    sp30maxo,sp30maxq
      real    uwamaxo,cwamaxo,wamaxq
      real    enmaxo
      real    wamaxcor	
c
c Quality values
c
      real    smra,smrv,smrd,smrwa
      real    smrsp03,smrsp10,smrsp30	
c
c Parameters for the realsp sorting routine
c
      real amax,vmax,dmax,wamax
      real sp03max,sp10max,sp30max

      integer iamax,ivmax,idmax,iwamax
      integer isp03max,isp10max,isp30max
      integer kamax,kvmax,kdmax,kwamax
      integer ksp03max,ksp10max,ksp30max

      real    avaa,avav,avad,avawa
      real    avasp03,avasp10,avasp30
c
c Parameters for uen routine
c
      real enmax
c
c
c ml100 and me100 and energy in ergs
c
c
      real    renergy,rml100,rme100,rml100corr
      real*8  dloga100, cisn_la100
c
c Arrays passed back and forth to retain last values calulcated
c
      real    rzx(2)
      real    rzya(2)
      real    rzyv(2)
      real    rzyd(2)
      real    rsp03(2)
      real    rsp10(2)
      real    rsp30(2)
      real    rzywa(2)
      real    rzyen(2)
c
c Input Waveform
c
      integer ismpls(MAX_SAMPLES_IN_TIME_WINDOW)

c
c
c
      real gwa,c1wa,c2wa
      real gsp03,c1sp03,c2sp03
      real gsp10,c1sp10,c2sp10
      real gsp30,c1sp30,c2sp30
      real amult

c
c Internal Working array
c

      real xr1,ar1,vr1,dr1,sp031,sp101,sp301,war1,enr1
      real xr2,ar2,vr2,dr2,sp032,sp102,sp302,war2,enr2
      real xr3,ar3,vr3,dr3,sp033,sp103,sp303,war3,enr3

      integer ndim2,j,kl
      real pi2,t03,t10,t30, pi, cen2a, cen2b, dist100

      parameter (pi2=6.28318531)
      parameter (t03=0.3,t10=1.0,t30=3.0)
      parameter (pi=3.141592)
      parameter (cen2a=1.96, cen2b=9.05)
      parameter (dist100=100.)
      parameter (ndim2=MAX_SAMPLES_IN_TIME_WINDOW)

      real ua, uv, ud, uwa, usp03, usp10, usp30, uen
      dimension  ua(ndim2)
      dimension  uv(ndim2)
      dimension  ud(ndim2)
      dimension  uwa(ndim2)
      dimension  usp03(ndim2)
      dimension  usp10(ndim2)
      dimension  usp30(ndim2)
      dimension  uen(ndim2)

c
c Convert time window to nw
c

      nw = nsamples

c
c These values are calculated once outside this routine
c   to reduce processing load of this program
c
	gwa  = agwa
	c1wa = ac1wa
	c2wa = ac2wa

	gsp03 = agsp03
        c1sp03 = ac1sp03
        c2sp03 = ac2sp03

	gsp10 = agsp10
        c1sp10 = ac1sp10
        c2sp10 = ac2sp10

	gsp30 = agsp30
        c1sp30 = ac1sp30
        c2sp30 = ac2sp30

	amult = aamult
c
c
c --- Check if this is first time through routine for this stream
c     It is up to the calling program to identify the first time
c     and then to identify that it is not the first time.
c
      if (ibegin.eq.0)  then
c
c Establish first two data points
c

      do kl=1,2 
        rzx(kl)=ismpls(kl)
      end do
c
c change the ibegin indicating initialization has been done
c
c
c
c First time through print out all the parameters in use
c
c      write(*,*) 'gf',gf
c      write(*,*) 'nw',nw
c      write(*,*) 'dt',dt
c      write(*,*) 'q',q
c      write(*,*) 'itype',itype
c      write(*,*) 'amlcor Z',amlcor
c      write(*,*) 'amecor Z',amecor

c      write(*,*) 'amult', amult
c      write(*,*) 'gwa', gwa
c      write(*,*) 'c1wa',c1wa
c      write(*,*) 'c2wa',c2wa

c      write(*,*) 'gsp03', gsp03
c      write(*,*) 'c1sp03',c1sp03
c      write(*,*) 'c2sp03',c2sp03

c      write(*,*) 'gsp10', gsp10
c      write(*,*) 'c1sp10',c1sp10
c      write(*,*) 'c2sp10',c2sp10

c      write(*,*) 'gsp30', gsp30
c      write(*,*) 'c1sp30',c1sp30
c      write(*,*) 'c2sp30',c2sp30

      ibegin=1

      end if

c
c Normal path starts here, transfering the first two samples into
c  working registers
c
c

      xr1=rzx(1)
      xr2=rzx(2)
      ar1=rzya(1)
      ar2=rzya(2)
      vr1=rzyv(1)
      vr2=rzyv(2)
      dr1=rzyd(1)
      dr2=rzyd(2)

      sp031=rsp03(1)
      sp032=rsp03(2)
      sp101=rsp10(1)
      sp102=rsp10(2)
      sp301=rsp30(1)
      sp302=rsp30(2)
	
      war1=rzywa(1)
      war2=rzywa(2)
      enr1=rzyen(1)
      enr2=rzyen(2)
c
c   read 1 data and compute the response (one step) using recursive filter
c
c
300   continue
      do j=1, nw

        xr3 = ismpls(j)

        call recres5(xr1,xr2,xr3,ar1,ar2,ar3,
     2 vr1,vr2,vr3,dr1,dr2,dr3,
     2 war1,war2,war3,
     2 sp031,sp032,sp033,
     2 sp101,sp102,sp103,
     2 sp301,sp302,sp303,		
     2 enr1,enr2,enr3,
     2 gf,dt,q,
     2 gwa,c1wa,c2wa,
     2 gsp03,c1sp03,c2sp03,
     2 gsp10,c1sp10,c2sp10,
     2 gsp30,c1sp30,c2sp30,
     2 itype)

        ua(j)=ar3
        uv(j)=vr3
        ud(j)=dr3
        uwa(j)=war3
        usp03(j) = sp033
        usp10(j) = sp103
        usp30(j) = sp303
        uen(j)=enr3
c
c End do loop which looped through all samples in time window
c
c
      end do
c
c
c
      call realsp(ua,nw,amax,kamax,avaa,smra)
      call realsp(uv,nw,vmax,kvmax,avav,smrv)
      call realsp(ud,nw,dmax,kdmax,avad,smrd)
      call realsp(uwa,nw,wamax,kwamax,avawa,smrwa)
      call realsp(usp03,nw,sp03max,ksp03max,avasp03,smrsp03)
      call realsp(usp10,nw,sp10max,ksp10max,avasp10,smrsp10)
      call realsp(usp30,nw,sp30max,ksp30max,avasp30,smrsp30)
      enmax=uen(nw)
      wamax=abs(wamax)

c if wamax of enmax is very small, it is set at a water level

      if (wamax .le. 1.0e-4) wamax = 0.0001
      if (enmax .le. 1.0e-12) enmax=1.0e-12

c
c Transfer the results to output parameters
c

      amaxo=amax
      iamax = kamax
      amaxq = smra

      vmaxo=vmax
      ivmax=kvmax
      vmaxq = smrv

      dmaxo=dmax
      idmax=kdmax
      dmaxq=smrd
c
c
c realsp produces spectral displacement. Why Hiroo Kanamori could not
C document this fact is unknown and a major embarassment!
c We want to put spectral acceleration in cm/(sec*sec) into ADA packets.
c Therefore we make the following conversions:
c
	sp03maxo=sp03max*((pi2/t03)**2)
	sp10maxo=sp10max*((pi2/t10)**2)
	sp30maxo=sp30max*((pi2/t30)**2)

	isp03max = ksp03max
	isp10max = ksp10max
 	isp30max = ksp30max

	sp03maxq = smrsp03*((pi2/t03)**2)
	sp10maxq = smrsp10*((pi2/t10)**2)
	sp30maxq = smrsp30*((pi2/t30)**2)
c
c
c
c
c Assign the uncorrected wa max as a return parameter
c
      uwamaxo = wamax
c
c Find the corrected wa max, and return it for use by Richter
c
      wamaxcor=wamax*10**amlcor
c
c
c
      cwamaxo=wamaxcor
      iwamax=kwamax
      wamaxq = smrwa
 
      enmaxo=enmax

c
c Copy the values into the return parameters
c
      rzx(1)=xr1
      rzx(2)=xr2
      rzya(1)=ar1
      rzya(2)=ar2
      rzyv(1)=vr1
      rzyv(2)=vr2
      rzyd(1)=dr1
      rzyd(2)=dr2
      rzywa(1)=war1
      rzywa(2)=war2

      rsp03(1)=sp031
      rsp03(2)=sp032
      rsp10(1)=sp101
      rsp10(2)=sp102
      rsp30(1)=sp301
      rsp30(2)=sp302

      rzyen(1)=0.
      rzyen(2)=0.

c
c Now the magnitude calculations
c
c Use the corrected wa max. This is a recent change, where recent is a
C relative term. It appears to mean some time before 2000.
c
c      rml100=alog10(wamaxcor*10.)+aloga0(dist100)
c
c     rml100=alog10(wamaxo*10.)+aloga0(dist100)+amlcor
c
c 2007/04/30, PNL: use CISN logAo functions to compute ml100:

      if (rml100corr .gt. 0.0) then
        rml100 = alog10(wamaxcor*10.0) + rml100corr
c        write(*,*) 'rml100corr=',rml100corr
      else
        dloga100 = cisn_la100()
        rml100 = alog10(wamaxcor*10.0) + real(dloga100)
      endif
c
      renergy=amult*3.0*enmaxo
      rme100=(alog10(renergy*amecor)-cen2b)/cen2a
      return
      end
