C> @file
C> @ingroup group_rapid_amplitude_data
C> @brief Calculate the input parameters to the realmag program.
c /***********************************************************
c 
c File Name :
c c1c2s.f
c 
c Programmer:
c Phil Maechling
c 
c Description:
c Calculate the input parameters to the realmag program.
c 
c Creation Date:
c 30 June 1998
c 
c 
c Usage Notes:
c 
c 
c 
c Modification History:
c 
c Added 40-sps coefficients that were computed by Hiroo Kanemori
c Pete Lombard, UCB 27 January, 2004
c 
c Added 200-sps coefficients for accelerometers
c Paul Friberg, ISTI 2005/05/05
c
c Replaced all coefficients with the ones used in jiggle,
c calculated by Allan Walter.
c Pete Lombard, UCB 15 August 2008
c
c Modified to allow WAS gain to be provided as argument  as it
c was fixed at 2080 for CISN
c Paul Friberg, ISTI, 2011/08/16
c
c Added 250- and 500-sps coefficients for accelerometers 
c and broadband, calculated by Allan Walter.
c Pete Lombard UCB 14 May 2012
c**********************************************************/

      subroutine c1c2s(
     2     icha,dt,iout,
     2     f0,h,
     2     g,c1,c2,
     2     hcrit,
     2     ramult,
     2     gwas)
c 
c Input Parameters
c 
c icha = 0 broadband
c icha = 1 fba
c icha = 2 40-80-100 sps broadband
c 
c 
c dt = seconds per sample
c 
c iout = 0 wa amplitude 
c iout = 1 0.3 period spectral peak
c iout = 2 1.0 period spectral peaks
c iout = 3 3.0 period spectral peaks
c 
c 
      integer icha,iout
      real dt
      real f0,h,hcrit
      real g, c1,c2
      real ramult,gwas

      real amp,amp0,amult

c 
c Fixed Parameters
c 
c
      parameter (pi2=6.28318531)
      parameter (hwa=0.8, fwa=1.25)
      parameter (hwab=0.65, fwab=1.35, gwab=3276.)
      parameter (rho=2.5, velocity=3.0, cqr0=0.49710, anqr0=1.0322)
      parameter (akqr0=0.0035, rref=8.0, pi=3.141592)
      parameter (cen2a=1.96, cen2b=9.05)


      dimension hi(64), f0i(64), gi(64)

c Following coefficients are provided by Allan Walter and Hiroo Kanamoori.
c Each row is for a different sample rate and instrument type
C From top to bottom these are: 
c 20 SPS Broadband
c 40 SPS Broadband
c 50 SPS Broadband
c 80 SPS Broadband
c 100 SPS Broadband
c 200 SPS Broadband
c 250 SPS Broadband NEW
c 500 SPS Broadband NEW
c 20 SPS Accelerometer
c 40 SPS Accelerometer
c 50 SPS Accelerometer
c 80 SPS Accelerometer
c 100 SPS Accelerometer
c 200 SPS Accelerometer
c 250 SPS Accelerometer NEW
c 500 SPS Accelerometer NEW
c The columns are for the four types of synthetic values. From left to
C right, they are:
c Wood-Anderson
c 0.3 sec period peak spectral velocity
c 1.0 sec period peak spectral velocity
c 3.0 sec period peak spectral velocity
      data f0i/
     $     1.4094, 3.2635, 1.0036, 0.33402,
     $     1.3422, 3.3381, 1.0029, 0.33370,
     $     1.3251, 3.3434, 1.0025, 0.33362,
     $     1.2980, 3.3455, 1.0017, 0.33351,
     $     1.2886, 3.3446, 1.0014, 0.33346,
     $     1.2692, 3.3405, 1.0007, 0.33345,
     $     1.2656, 3.3393, 1.0006, 0.33333,
     $     1.2578, 3.3365, 1.0003, 0.33370,
     $     1.3829, 3.2628, 1.0036, 0.33401,
     $     1.3357, 3.3376, 1.0029, 0.33370,
     $     1.3210, 3.3431, 1.0025, 0.33362,
     $     1.2963, 3.3453, 1.0017, 0.33351,
     $     1.2876, 3.3445, 1.0014, 0.33346,
     $     1.2692, 3.3405, 1.0007, 0.33344,
     $     1.2654, 3.3393, 1.0006, 0.33333,
     $     1.2578, 3.3365, 1.0003, 0.33370/
      data hi/
     $      0.63382,  -0.46005,  -0.10745,  -0.0024322,
     $      0.74306,  -0.21115,  -0.028609,  0.023834, 
     $      0.75820,  -0.15931,  -0.012856,  0.029074, 
     $      0.77721,  -0.080957,  0.010752,  0.036935, 
     $      0.78262,  -0.054752,  0.018613,  0.039553, 
     $      0.79251,  -0.0023261, 0.034317,  0.044768, 
     $      0.79384,   0.0081515, 0.037453,  0.045795,
     $      0.79702,   0.029091,  0.043749,  0.047835,
     $      0.68405,  -0.45854,  -0.10705,  -0.0023133,
     $      0.75422,  -0.21077,  -0.028515,  0.023861, 
     $      0.76517,  -0.15907,  -0.012796,  0.029092, 
     $      0.77980,  -0.080868,  0.010775,  0.036943, 
     $      0.78427,  -0.054694,  0.018628,  0.039555, 
     $      0.79251,  -0.0023125, 0.034319,  0.044785, 
     $      0.79407,   0.0081605, 0.037454,  0.045778,
     $      0.79711,   0.029093,  0.043754,  0.047952/

c data gi/3110.,0.875,0.988,0.986,2999.,0.990,0.998,0.998,
c 2 2963.,0.996,0.988,0.999,2999.,1.0,1.,1.,2963.,1.,1.,1./
c 
c replaced above array with one below without GAIN factor multipled
c through for elements 1, 5, 9, 13, etc., which are used for WA
c calculations.

      data gi/
     $     1.2022,  0.89647, 0.99718, 1.0020,  
     $     1.1378,  0.98811, 1.0034,  1.0018,  
     $     1.1144,  0.99670, 1.0034,  1.0015,  
     $     1.0748,  1.0037,  1.0028,  1.0012,  
     $     1.0605,  1.0045,  1.0024,  1.0010,  
     $     1.0310,  1.0037,  1.0014,  1.0004,  
     $     1.0248,  1.0032,  1.0013,  1.0002,
     $     1.0125,  1.0019,  1.0005,  0.99945,
     $     1.2147,  0.87314, 0.99896, 1.0034,  
     $     1.1398,  0.98158, 1.0037,  1.0021,  
     $     1.1156,  0.99247, 1.0036,  1.0017,  
     $     1.0750,  1.0020,  1.0029,  1.0013,  
     $     1.0607,  1.0034,  1.0025,  1.0010,  
     $     1.0309,  1.0035,  1.0014,  1.0007,
     $     1.0248,  1.0031,  1.0013,  0.99998,
     $     1.0125,  1.0018,  1.0006,  0.99759/

c 
c 
c First find the amult value
c Calculate this once for each magnitude channel
c 

c 
c Calculate these once
c 
      
      amp0=qr1(rref,cqr0,anqr0,akqr0)
      amp=qr1(sqrt(10000.+rref**2),cqr0,anqr0,akqr0)
      amult=rho*velocity*4.*3.141592*rref**2*amp0**2/(4.*amp**2)
      amult=amult*1.e15

c 
c Assign to the return variables
c 
      ramult = amult


c 
c Convert period to frequency
c 

      isps=int(0.5 + 1.0/dt)
c 
c 
c 
      if(isps .eq. 20 .or. isps .eq. 40 .or. isps .eq. 50 .or.
     $     isps .eq. 80 .or. isps .eq. 100 .or. isps .eq. 200 .or.
     $     isps .eq. 250 .or. isps .eq. 500)  then
         if((icha .eq. 0 .or. icha .eq. 2) .and. isps .eq. 20)  then ! Broadband channels
            if(iout .eq. 0)  then ! Wood-Anderson
               h=hi(1)
               f0=f0i(1)
               g=gi(1)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(2)
               f0=f0i(2)
               g=gi(2)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(3)
               f0=f0i(3)
               g=gi(3)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(4)
               f0=f0i(4)
               g=gi(4)
            end if
         elseif((icha .eq. 0 .or. icha .eq. 2) .and. isps .eq. 40)  then
            if(iout .eq. 0)  then ! Wood - Anderson
               h=hi(5)
               f0=f0i(5)
               g=gi(5)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(6)
               f0=f0i(6)
               g=gi(6)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(7)
               f0=f0i(7)
               g=gi(7)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(8)
               f0=f0i(8)
               g=gi(8)
            end if
         elseif((icha .eq. 0 .or. icha .eq. 2) .and. isps .eq. 50)  then
            if(iout .eq. 0)  then ! Wood - Anderson
               h=hi(9)
               f0=f0i(9)
               g=gi(9)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(10)
               f0=f0i(10)
               g=gi(10)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(11)
               f0=f0i(11)
               g=gi(11)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(12)
               f0=f0i(12)
               g=gi(12)
            end if
         elseif((icha .eq. 0 .or. icha .eq. 2) .and. isps .eq. 80) then
            if(iout .eq. 0)  then ! Wood - Anderson
               h=hi(13)
               f0=f0i(13)
               g=gi(13)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(14)
               f0=f0i(14)
               g=gi(14)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(15)
               f0=f0i(15)
               g=gi(15)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(16)
               f0=f0i(16)
               g=gi(16)
            end if
         elseif((icha .eq. 0 .or. icha .eq. 2) .and. isps .eq. 100) then
            if(iout .eq. 0)  then ! Wood - Anderson
               h=hi(17)
               f0=f0i(17)
               g=gi(17)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(18)
               f0=f0i(18)
               g=gi(18)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(19)
               f0=f0i(19)
               g=gi(19)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(20)
               f0=f0i(20)
               g=gi(20)
            end if
         elseif((icha .eq. 0 .or. icha .eq. 2) .and. isps .eq. 200) then
            if(iout .eq. 0)  then ! Wood-Anderson
               h=hi(21)
               f0=f0i(21)
               g=gi(21)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(22)
               f0=f0i(22)
               g=gi(22)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(23)
               f0=f0i(23)
               g=gi(23)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(24)
               f0=f0i(24)
               g=gi(24)
            end if
         elseif((icha .eq. 0 .or. icha .eq. 2) .and. isps .eq. 250) then
            if(iout .eq. 0)  then ! Wood-Anderson
               h=hi(25)
               f0=f0i(25)
               g=gi(25)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(26)
               f0=f0i(26)
               g=gi(26)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(27)
               f0=f0i(27)
               g=gi(27)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(28)
               f0=f0i(28)
               g=gi(28)
            end if
         elseif((icha .eq. 0 .or. icha .eq. 2) .and. isps .eq. 500) then
            if(iout.eq.0)  then ! Wood-Anderson
               h=hi(29)
               f0=f0i(29)
               g=gi(29)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(30)
               f0=f0i(30)
               g=gi(30)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(31)
               f0=f0i(31)
               g=gi(31)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(32)
               f0=f0i(32)
               g=gi(32)
            end if
         elseif(icha .eq. 1 .and. isps .eq. 20)  then ! Accelerometer channels
            if(iout .eq. 0)  then ! Wood-Anderson
               h=hi(33)
               f0=f0i(33)
               g=gi(33)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(34)
               f0=f0i(34)
               g=gi(34)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(35)
               f0=f0i(35)
               g=gi(35)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(36)
               f0=f0i(36)
               g=gi(36)
            end if
         elseif(icha .eq. 1 .and. isps .eq. 40)  then ! Accelerometer channels
            if(iout .eq. 0)  then ! Wood - Anderson
               h=hi(37)
               f0=f0i(37)
               g=gi(37)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(38)
               f0=f0i(38)
               g=gi(38)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(39)
               f0=f0i(39)
               g=gi(39)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(40)
               f0=f0i(40)
               g=gi(40)
            end if
         elseif(icha .eq. 1 .and. isps .eq. 50)  then
            if(iout .eq. 0)  then ! Wood - Anderson
               h=hi(41)
               f0=f0i(41)
               g=gi(41)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(42)
               f0=f0i(42)
               g=gi(42)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(43)
               f0=f0i(43)
               g=gi(43)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(44)
               f0=f0i(44)
               g=gi(44)
            end if
         elseif(icha .eq. 1 .and. isps .eq. 80)  then
            if(iout .eq. 0)  then ! Wood - Anderson
               h=hi(45)
               f0=f0i(45)
               g=gi(45)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(46)
               f0=f0i(46)
               g=gi(46)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(47)
               f0=f0i(47)
               g=gi(47)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(48)
               f0=f0i(48)
               g=gi(48)
            end if
         elseif(icha .eq. 1 .and. isps .eq. 100)  then
            if(iout .eq. 0)  then ! Wood - Anderson
               h=hi(49)
               f0=f0i(49)
               g=gi(49)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(50)
               f0=f0i(50)
               g=gi(50)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(51)
               f0=f0i(51)
               g=gi(51)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(52)
               f0=f0i(52)
               g=gi(52)
            end if
         elseif(icha .eq. 1 .and. isps .eq. 200)  then
            if(iout .eq. 0)  then ! Wood - Anderson
               h=hi(53)
               f0=f0i(53)
               g=gi(53)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(54)
               f0=f0i(54)
               g=gi(54)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(55)
               f0=f0i(55)
               g=gi(55)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(56)
               f0=f0i(56)
               g=gi(56)
            end if
         elseif(icha .eq. 1 .and. isps .eq. 250)  then
            if(iout .eq. 0)  then ! Wood - Anderson
               h=hi(57)
               f0=f0i(57)
               g=gi(57)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(58)
               f0=f0i(58)
               g=gi(58)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(59)
               f0=f0i(59)
               g=gi(59)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(60)
               f0=f0i(60)
               g=gi(60)
            end if
         elseif(icha .eq. 1 .and. isps .eq. 500)  then
            if(iout .eq. 0)  then ! Wood - Anderson
               H=hi(61)
               f0=f0i(61)
               g=gi(61)*gwas
            elseif(iout .eq. 1)  then ! 0.3 period spectral peaks
               h=hi(62)
               f0=f0i(62)
               g=gi(62)
            elseif(iout .eq. 2)  then ! 1.0 period spectral peaks
               h=hi(63)
               f0=f0i(63)
               g=gi(63)
            elseif(iout .eq. 3)  then ! 3.0 period spectral peaks
               h=hi(64)
               f0=f0i(64)
               g=gi(64)
            end if
         end if
         wdt=2.*3.1415926*f0*dt
         c1=1.+h*wdt
         c2=1.+2.*h*wdt+wdt**2
         hcrit=-wdt/2.
         return
      end if
      write(*,*) 'isps must be either 20, 40, 80, 100, 200, 250 or 500'
      stop
      end
