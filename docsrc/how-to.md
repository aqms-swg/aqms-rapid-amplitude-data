How to compile and install the rapid amplitude generator {#how-to-compile-install-rad2}
========================================================  

# Pre-requisites

RAD depends on aqms-libs, so ensure that they are compiled and available before hand.

# Compile

Run  

``make -f Makefile  ``

This will generate two binaries named, *rad2* and *msrad*


# Install

Copy the binary *rad2* to desired location. Copy configuration file *rad2.cfg* and modify as desired.