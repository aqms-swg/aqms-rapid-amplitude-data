# rad2 {#man-rad2}

Realtime Amplitude Data generator release 2

## PAGE LAST UPDATED ON

2020-02-28

## NAME

rad2

## VERSION and STATUS

v0.2.9 2013-01-17  
status : ACTIVE  

## PURPOSE

Rad2 is a module that takes raw waveform data from Broadband and Strong-Motion instruments (FBAs) and computes amplitude information for a specific length time window in real time. For SCSN and NCSN this time window has been 5 seconds. The idea behind rad2 is to compute real time amplitude and magnitude snapshot information for a given time duration so that amplitude computations for ShakeMap and magnitude calculators can quickly extract the needed amplitudes rapidly at the time the information is needed (rather than have to reduce down a large volume of waveform data). The algorithm for performing the computations that produce the amplitude data are from Hiroo Kanamori and are recursive filters designed for specific types of instruments and sampling rates. The sampling rates currently allowed are (20, 40, 80, 100, or 200).

Rad2 reads its raw waveform data from the WDA (waveform data area) that is written to by ew2wda (previously via the cs2wda module) and writes the amplitudes computed to the ADA (Amplitude Data Area) for downstream modules to retrieve and process. The ADA must be created with the makeada program before running and the GCDA_FILE environment variable must be set so that the key mappings may be found.

The amplitude values being computed are written into a C++ structure that contains information about peak acceleration, peak velocity, peak displacement, response spectral values for 0.3, 1.0, and 3.0 Hertz, peak Wood Anderson amplitudes, ML100 (ML values normalized at 100Km distance), and ME100 for the duration of the window. In addition a number of flags about the quality of the data and also signal to noise computations are provided for determining if the data used were complete and good (in the sense that none were missing or potentially clipped).

## HOW TO RUN
```
[aparnab@atlantic bin]$ ./rad2

usage: ./rad2 <config file>

./rad2:  Version v0.2.9 2013-01-17
[aparnab@atlantic bin]$ 
```
## CONFIGURATION FILE

The configuration file is named rad2.cfg and has the following arguments. Format of the description is:

parameter_name *data_type* *default_value*  
description  

Logfile *string*  
The path with a logfile prefix where daily logs will be stored.  

LoggingLevel *number*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  

LockFile *string*  
Full path to lock file. If you only wish one instance of this module to run, then the lockfile name should be unique per instance. 

ConsoleLog *number*  
Boolean. Set to 1 to log console messages to log file.  

ProgramName *string* *RAD*  
The unique name to allow this module to connect to CMS and be identifed.   

ProgName *string* *RAD*  
???

WDAKeyName *string*  
Shared memory lookup key ???  

ADAKeyName *string*  
Shared memory lookup key ???  

SecsInWindow *number*  
the length in seconds of the window to compute the amplitude values. Has not been tried for other than 5 second blocks...change at your peril.  

QualityB *float*  
A quality factor applied in the guts of Hiroo Kanamori's code for the recursive algorithm. Don't mess with this!  

QualityS *float*  
A quality factor applied in the guts of Hiroo Kanamori's code for the recursive algorithm. Don't mess with this!  

QualityP *float*  
A quality factor applied in the guts of Hiroo Kanamori's code for the recursive algorithm. Don't mess with this!  

MinAccel *float*  
A minimum cm/s/s for the data from strong motion instruments to be declared usable if the rms is above this.

MaxVelocity *float*  
A maximum cm/s for the data from broad band (velocity) instruments at which it is declared clipped.  

LTA_windowBB *number*  
An integer number of seconds for the LTA for the SNR computation. Should be a mulitple of the SecsInWindow 
LTA_windowSM *number*  
An integer number of seconds for the STA for the SNR computation. Should be a mulitple of the SecsInWindow  
ewLogA0 *string*  
OPTIONAL. The absolute path to an Earthworm localmag Richter LogA0 relation file. See the ew docs on [localmag command](http://www.earthwormcentral.org/documentation4/cmd/localmag_cmd.html). If this is not set, then the CISN LogA0 relation will be used. This is only used for the ML100 computation by Rad.  

WAS_gain *float*  
OPTIONAL. Set the WAS gain value, DEFAULTS to 2080 if not set.  

Include *string*  
Include external files with path prefixed.  

**Database connection parameters**

DBService *string*  
The name of the database being used.  

DBUser *string*  
The name of the database user account.  

DBPasswd *string*  
The password for the database user account specified by DBUser.  
 
*A sample configuration file is shown below*    
```
Logfile         ../rt/logs/rad_ae
LockFile        ../rt/locks/rad_ae.lock
ConsoleLog      0
LoggingLevel    1
ProgramName     RAD_AE

# the next options says where to read the config info from Database or Files 
#Use Files - this should be deprecated
Use     Database

# if Use is db, then These are the program into config_channel table lookups
ProgName    RAD_AE
WDAKeyName  WDA_AE_KEY
ADAKeyName  ADA_KEY

# Required Configs
Include rad_common.cfg

#
# Database configuration settings
#
Include ../db/db_info.d
```
*where rad_common.cfg contains:*  
```
# these params are common to all rad instances
SecsInWindow    5
QualityB        0.998
QualityS        0.998
QualityP        0.998
MinAccel        0.75
MaxVelocity     1.0
LTA_windowBB    60
LTA_windowSM    30

# a test of the ew logA0 func
#ewLogA0  Richter.tab

# set the gain value for the ML WAS if you care to change it from 2080
WAS_gain 2080
```   
*and db_info.d contains:*
```
DBService       some service name
DBUser          some user
DBPasswd        some password
```

## DEPENDENCIES

The rad2 module depends on the WDA and ADA being created. A program must be feeding the WDA.

To get its channels, rad2 relies on the Oracle Database tables named simple_response (for gain values) and channel_data for sample rate. It also finds its list of channels to operate on from the program and config_channel tables.

This module does not use CMS.

## MAINTENANCE

Look at the log files and use the accmon to check on a particular set of outputs. Use the scang module to search the WDA and ADA for problem channels.

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-rapid-amplitude-data/issues

## MORE INFORMATION

ew2wda, [gcda utils](https://gitlab.com/aqms-swg/aqms-gcda), [trimag](https://gitlab.com/aqms-swg/aqms-rapid-magnitude), [ampgen](https://gitlab.com/aqms-swg/aqms-amplitude-generator), and [conlog](https://gitlab.com/aqms-swg/aqms-tools).

"Continuous Monitoring of Ground-Motion Parameters" by Kanamori, Maechling & hauksson; BSSA vol 89, pp 311-316; February 1999.

