/** @file
 * @ingroup group_rapid_amplitude_data
 * @brief Header file for DatabaseMsRad.C
 */
/***********************************************************

File Name :
        DatabaseMsRad.h

Original Author:
        Pete Lombard

Description:


Creation Date:
        14 December 2005

Modification History:


Usage Notes:


**********************************************************/

#ifndef database_msrad_H
#define database_msrad_H

// Various include files
#include "chan.h"
#include "Database.h"


class DatabaseMsRad : public Database
{
 private:

 protected:

 public:
    DatabaseMsRad();
    DatabaseMsRad(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseMsRad();

    int getChanParams(struct channel_definition_type &chan, TimeStamp &time);
};


#endif
