/** @file
 * @ingroup group_rapid_amplitude_data
 * @brief Header file for cvert.C
 */
/***********************************************************

File Name :
	cvert.h	

Programmer:
	Phil Maechling

Description:
	Converter object. This converts waveforms to
	amplitudes.


Creation Date:
	8 August 1996


Usage Notes:



Modification History:
	5 May 1998 - Adding calculations of spectral peaks.
	21 Dec 2007 - updated to support leap seconds and new tntime classes


**********************************************************/
#ifndef cvert_H
#define cvert_H

#include <list>

#include "dwriter.h"
#include "dcmon.h"
#include "ddcr.h"
#include "AmplitudeADA.h"
#include "chan.h"
#include "samples.h"
#include "DataBuffer.h"
#include "TimeStamp.h"

typedef std::list<double> LTAList;

const int MAX_SECONDS_BEHIND_REALTIME=180;
const int MAX_MISSING_SAMPLES = 20;

const int WA_TYPE = 0;
const int SPECTRAL_PEAK_0_3_TYPE = 1;
const int SPECTRAL_PEAK_1_0_TYPE = 2;
const int SPECTRAL_PEAK_3_0_TYPE = 3;


class Convert_Waves_to_Amplitudes
{

  public:

  Convert_Waves_to_Amplitudes(Defined_Data_Channel_Reader<int>*  wavereader,
	        Data_Channel_Writer<struct amplitude_data_type>* ampwriter,
                char* netid,
                int   seconds_in_exam_window, 
  	        float q_factor,
		float minimum_acceleration,
                float maximum_velocity, 
	        float lta_window_bb, float lta_window_sm, float ml100_corr, float gwas);
             
  Convert_Waves_to_Amplitudes(
		struct channel_definition_type chan,	      
                Data_Channel_Writer<struct amplitude_data_type>* ampwriter,
                char* netid,
                int   seconds_in_exam_window, 
  	        float q_factor,
		float minimum_acceleration,
                float maximum_velocity, 
	        float lta_window_bb, float lta_window_sm, float ml100_corr, float gwas);
             
  ~Convert_Waves_to_Amplitudes();

  int Set_Debug_On(int debug_flag);
  int enough_waveform_for_conversion();
  int enough_waveform_for_conversion(DataBuffer &buf);
  int convert_waveform_to_amplitudes();

  private:

  int find_amplitudes(struct amplitude_data_type& amps);
  int write_amplitudes(struct amplitude_data_type& amps);
  int _calculate_noise_levels();
  int _update_lta();
  void _roundtime( TimeStamp& ts );

  float get_snr( int *waveform, int num_samples );

  Defined_Data_Channel_Reader<int>* p_wave_ptr;
  Data_Channel_Writer<struct amplitude_data_type>* p_amp_ptr;

  //
  // These three together and should possible be a structure
  //
  int     waveform[MAX_SAMPLES_IN_TIME_WINDOW];
  TimeStamp timestamps[MAX_SAMPLES_IN_TIME_WINDOW];
  TimeStamp p_wave_start_time;

  //
  // These are program oriented values, one for each version of rad
  //

  char  p_network_id[10];
  int   p_samples_in_exam_window;
  int   p_seconds_in_exam_window;
  float p_minimum_acceleration;
  float p_maximum_velocity;
  int   p_samples_extracted;
  float gwas;

  //
  // These are channel specific stuff that are filled in
  // then referenced.
  //


  float p_q_factor;
  float p_sample_period;
  int   p_instrument_type;
  int   p_clip_counts;
  float p_gain_factor;
  float p_ml_correction;
  float p_me_correction;
   
  float p_ml_100_corr;

  //
  // These are unique to each object
  //

  TimeStamp p_next_time;  // start time of the next bunch of samples
  int p_ibegin;
  int p_fullwin;	/* is the number of samples complete, 0=no, 1=yes */

  // related to noise and variance calculation
  float p_last_bias;
  float p_latest_bias;
  float p_raw_noise_level;
  int p_ada_flags; 	/* see the ADA flags  AmplitudesADA.h*/

  float p_lta_window_bb;	// size of lta in seconds for BroadBand
  float p_lta_window_sm;	// size of lta in seconds for StrongMotion
  double p_last_lta;	// last lta value calculated
  float  p_lta_seconds; // actual size of filled lta in seconds
  int p_lta_size_bb;	// number of windows needed for full LTA for Broadband
  int p_lta_size_sm;	// number of windows needed for full LTA for Strong Motion
  LTAList lta_list;	// list of previous lta values

  // residual data for recursive filtering
  float p_zx[2];
  float p_zya[2];
  float p_zyv[2];
  float p_zyd[2];
  float p_zywa[2];
  float p_zyen[2];
  float p_sp_0_3[2];
  float p_sp_1_0[2];
  float p_sp_3_0[2];

  float p_gwa;
  float p_c1wa;
  float p_c2wa;

  float p_gsp_0_3;
  float p_c1sp_0_3;
  float p_c2sp_0_3;

  float p_gsp_1_0;
  float p_c1sp_1_0;
  float p_c2sp_1_0;

  float p_gsp_3_0;
  float p_c1sp_3_0;
  float p_c2sp_3_0;

  float p_amult;

  // These are apparently calculated, but then never used.
  // However, they might be someday, so
  // we carry them along.
  //

  float p_f0;
  float p_h;
  float p_hcrit;

  int p_debug; 
};

#endif
