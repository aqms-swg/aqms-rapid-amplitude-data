########################################################################
#
# Makefile     : rad
#
# Author       : Paul Friberg
#
# Last Revised : November 11, 2003
# History      : 
#
#
########################################################################

include $(DEVROOT)/shared/makefiles/Make.includes

BIN	= rad2 msrad

INCL	= $(RTSTDINCL)
LIBS	= $(RTSTDLIBS_NOCMS) -lgcda -lhk -lew -lgfortran
#DEBUG_FLAGS	= -DDEBUG_WRITE
#DEBUG_FLAGS	= -DDEBUG_CLIP
FFLAGS 		= -g

# rad2 is the new RTApplication based rad
RAD2OBJS = Main.o cvert.o Rad.o c1c2s.o recres5.o rmag31.o  DataBuffer.o

MSRADOBJS = msrad.o cvert.o c1c2s.o recres5.o rmag31.o DataBuffer.o \
   DatabaseMsRad.o

LTOBJS = ListTester.o

########################################################################

all:$(BIN)

rad2: $(RAD2OBJS)
	$(CC) $(DEBUG_FLAGS) $(CFLAGS) $(RAD2OBJS) -o $@ $(LIBS)

msrad: $(MSRADOBJS)
	$(CC) $(DEBUG_FLAGS) $(CFLAGS) $(MSRADOBJS) -o $@ $(LIBS)

#ListTester: $(LTOBJS)
#	$(CC) $(CFLAGS) $(LTOBJS) -o $@ $(LIBS)

.C.o: 
	$(CC) $(DEBUG_FLAGS) $(CFLAGS) $(INCL) -c $<  

.f.o:
	${FF} $(FFLAGS) -c $<

clean:
	-rm -f *.o *~ core $(BIN)
	-rm -f -r ./SunWS_cache
