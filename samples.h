/** @file
 * @ingroup group_rapid_amplitude_data
 * @brief This defines the max samples passed from calling program to realmag31.f
*/
/***********************************************************

File Name :
	samples.h

Programmer:
	Phil Maechling

Description:
	This defines the max samples passed from calling
	program to realmag31.f

	This MUST match the number in samples.inc, which
	is included by realmag1.f


Creation Date:
	7 August 1996


Usage Notes:



Modification History:



**********************************************************/

#ifndef samples_H
#define samples_H

const int MAX_SAMPLES_IN_TIME_WINDOW = 3000;

#endif
