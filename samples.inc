C> @file
C> @ingroup group_rapid_amplitude_data
C> @brief This defines the number of samples to be input to rmag31.f
c/***********************************************************
c
cFile Name :
c
c
cProgrammer:
c	Phil Maechling
c
cDescription:
c   	This defines the number of samples to be input to
c       realmag31.f at a single call. You want it big enough
c       but not too big.
c
c       This defines the number of samples in an array.
c
c
cCreation Date:
c	7 August 1996
c
c
cUsage Notes:
c	This must match samples.h which is the calling programs
c       understanding of the interface.
c
c
c
cModification History:
c
c
c
c**********************************************************/
c
c
c Define 30 seconds of 100 samples per second as max time period
c for this version
c
      integer MAX_SAMPLES_IN_TIME_WINDOW
      PARAMETER (MAX_SAMPLES_IN_TIME_WINDOW = 3000)


