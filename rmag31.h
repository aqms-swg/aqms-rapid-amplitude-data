/** @file
 * @ingroup group_rapid_amplitude_data
 * @brief Header file for rmag31.f
 */
/***********************************************************

File Name :
     rmag31.h

Programmer:
	Phil Maechling

Description:
 	This a modification of realmag2.f, hence realmag3.f
        This is a single component version, hence realmag31.f
	This is for a single component of data.
	The samples are passed in as an array.

Creation Date:
	5 August 1996

 Usage Notes:

 Note that broad band is assigned = 0 strong motion = 1


 Modification History:

  18 May 1998
	Added calculation of spectral peaks.

   24 March 1997 
	Added return of both uncorrected and correted wa amp.


*****************************************************************/
#ifndef rmag31_H
#define rmag31_H

#ifdef __cplusplus

extern "C" 
{

void realmag31_(int* ibegin,
          int* nsamples,
          float* dt,
          float* q,
          int* itype,
          float* agwa,float* ac1wa,float* ac2wa,
          float* agsp03,float* ac1sp03,float* ac2sp03,
          float* agsp10,float* ac1sp10,float* ac2sp10,
          float* agsp30,float* ac1sp30,float* ac2sp30,
          float* aamult,
          float* gf, float* amlcor, float* amecor,
          float rzx[],float rzya[],float rzyv[],
          float rzyd[],float rzywa[],float rzyen[],
          float rsp03[], float rsp10[], float rsp30[],
          float* amaxo, float* amaxq, int* iamax,
          float* vmaxo,float* vmaxq,int* ivmax,
          float* dmaxo,float* dmaxq,int* idmax,
          float* sp03maxo,float* sp03maxq,int* isp03max,
          float* sp10maxo,float* sp10maxq,int* isp10max,
          float* sp30maxo,float* sp30maxq,int* isp30max,
          float* uwamaxo,float* cwamaxo,float* wamaxq,int* iwamax,
          float* enmaxo,
          float* renergy,
          float* rml100,
          float* rme100,
          float* rml100corr,
          int sampls[]);
}

#else

realmag31_(int* ibegin,
          int* nsamples,
          float* dt,
          float* q,
          int* itype,
          float* agwa,float* ac1wa,float* ac2wa,
          float* agsp03,float* ac1sp03,float* ac2sp03,
          float* agsp10,float* ac1sp10,float* ac2sp10,
          float* agsp30,float* ac1sp30,float* ac2sp30,
          float* aamult,
          float* gf, float* amlcor, float* amecor,
          float rzx[],float rzya[],float rzyv[],
          float rzyd[],float rzywa[],float rzyen[],
          float rsp03[], float rsp10[], float rsp30[],
          float* amaxo, float* amaxq, int* iamax,
          float* vmaxo,float* vmaxq,int* ivmax,
          float* dmaxo,float* dmaxq,int* idmax,
          float* sp03maxo,float* sp03maxq,int* isp03max,
          float* sp10maxo,float* sp10maxq,int* isp10max,
          float* sp30maxo,float* sp30maxq,int* isp30max,
          float* uwamaxo,float* cwamaxo,float* wamaxq,int* iwamax,
          float* enmaxo,
          float* renergy,
          float* rml100,
          float* rme100,
          int sampls[]);

#endif
#endif
