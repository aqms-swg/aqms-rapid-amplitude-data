C> @file
C> @ingroup group_rapid_amplitude_data
C> @brief Online version of Hiroo Kanamori's recres5.f
c/***********************************************************
c
cFile Name :
c
c
cProgrammer:
c	Phil Maechling
c
cDescription:
c	Online version of Hiroo Kanamori's recres5.f
c
cCreation Date:
c	30 June 1998
c
c
cUsage Notes:
c
c
c
cModification History:
c
c
c
c**********************************************************/
c


      subroutine recres5(x1,x2,x3,a1,a2,a3,
     2     v1,v2,v3,d1,d2,d3,
     2     wa1,wa2,wa3,
     2     sp031,sp032,sp033,
     2     sp101,sp102,sp103,
     2     sp301,sp302,sp303,
     3     en1,en2,en3,
     2     gf,dt,q,
     2     gwa,c1wa,c2wa,
     4     g03,c103,c203,
     2     g10,c110,c210,
     2     g30,c130,c230,
     2     itype)

      IMPLICIT NONE
c
c Declare the variable types
c
      real x1,x2,x3
      real a1,a2,a3
      real v1,v2,v3
      real d1,d2,d3
      real wa1,wa2,wa3
      real sp031,sp032,sp033
      real sp101,sp102,sp103
      real sp301,sp302,sp303
      real en1,en2,en3
      real gf,dt,q
      real gwa,c1wa,c2wa
      real g03,c103,c203
      real g10,c110,c210
      real g30,c130,c230
      real b0
      integer itype
c
c
c itype = 0 broadband
c itype = 1 fba
c itype = 2 80-100 sps broadband
c
c  subroutine to process fba  and vbb to generate a, v, d, wa,
c  sp03, sp10, sp3, and en
c  simple high-pass filter is used to avoid drift
c  fba output is assumed to "acceleration", and vbb output
c  is assumed to be "velocity".  Hiroo Kanamori, 6/14/1996
c  at entry  x1, x2, and x3 are given.  a1, a2, v1, v2,
c  d1, d2, wa1, wa2, sp031, sp032, sp11, sp102, sp301, sp302,
c  en1, en2 are given,
c  then a3, v3, d3, wa3, sp033, sp103, sp303, and en3 are computed
c  at exit x1 is replaced with x2 and x3 is replaced with x3 etc.
c  q is a high-pass filter constant which is slightly smaller than 1.
c  modified from recres3, April, 1998

      b0=2./(1+q)
      
      if(itype.eq.1) then
         a3=(x3-x2)/(b0*gf)+q*a2
         v3=(a3+a2)*dt/(2.*b0)+q*v2
         wa3=(a3*gwa*dt**2+2.*c1wa*wa2-wa1)/c2wa
         sp033=(g03*a3*dt**2+2.*c103*sp032-sp031)/c203
         sp103=(g10*a3*dt**2+2.*c110*sp102-sp101)/c210
         sp303=(g30*a3*dt**2+2.*c130*sp302-sp301)/c230

      else if (itype.eq.0.or.itype.eq.2)  then
         a3=(x3-x2)/(dt*gf)
         wa3=((x3-x2)*(gwa/gf)*dt+2.*c1wa*wa2-wa1)/c2wa
         sp033=((g03/gf)*(x3-x2)*dt+2.*c103*sp032-sp031)/c203
         sp103=((g10/gf)*(x3-x2)*dt+2.*c110*sp102-sp101)/c210
         sp303=((g30/gf)*(x3-x2)*dt+2.*c130*sp302-sp301)/c230
         v3=(x3-x2)/(gf*b0)+q*v2
      else
         write(*,*) 'itype must be either 0, 1 or 2'
         stop
      end if
      d3=(v3+v2)*dt/(2.*b0)+q*d2
      en3=en2+(v3**2+v2**2)*dt/2.
      x1=x2
      x2=x3
      a1=a2
      a2=a3
      v1=v2
      v2=v3
      d1=d2
      d2=d3
      wa1=wa2
      wa2=wa3
      sp031=sp032
      sp032=sp033
      sp101=sp102
      sp102=sp103
      sp301=sp302
      sp302=sp303
      en1=en2
      en2=en3
      return
      end
