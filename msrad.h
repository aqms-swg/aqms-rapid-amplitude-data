/** @file
 * @ingroup group_rapid_amplitude_data
 * @brief Header file for msrad.C
 */
/***********************************************************

File Name :
        msrad.h

Original Author:
        Pete Lombard

Description:


Creation Date:
        12 December 2005

Modification History:
	21 Dec 2007 - updated to support leap seconds and new tntime classes


Usage Notes:


**********************************************************/

#ifndef rad_H
#define rad_H

#include <iostream>
#include <vector>
#include <cstdio>
#include <cstring>
#include <cstdlib>

#include "qlib2.h"
#include "RetCodes.h"
#include "seismic.h"
#include "gcda.h"
#include "dwriter.h"
#include "dreader.h"
#include "dchan.h"
#include "chan.h"
#include "AmplitudeADA.h"
#include "cvert.h"
#include "chancfg.h"
#include "nscl.h"
#include "ewLogA0.h"


#include "Application.h"
#include "LockFile.h"
#include "DataBuffer.h"
#include "DatabaseMsRad.h"

class MsRad : public Application 
{
 private:
    int seconds_in_exam_window;
    float q_b_factor;
    float q_s_factor;
    float q_p_factor;
    float minimum_acceleration;
    float maximum_velocity;
    float lta_window_bb;
    float lta_window_sm;
    char lock_file_name[MAXSTR];
    LockFile *lock;

    ewLogA0  *ew;	// used for ew LogA0 files
    float ml100_correction;	// computed from ew, or -9.0 for not used
    float was_gain;	// Wood Anderson Synth gain, defaults to 2080 for CISN

    char ada_key_name[GCDA_KEY_NAME_LEN];

    char progname[MAXSTR];    // used for RAD lookup in program/config_cannel tables
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    int dbinterval;
    int dbretries;

    Generic_CDA<struct amplitude_data_type> *ada;

    void _processStream(FILE *input, char *infile, DatabaseMsRad &db);
    int _processData(DataBuffer &buf, Convert_Waves_to_Amplitudes *cvert);
    int _lookupChan(DATA_HDR *hdr, Convert_Waves_to_Amplitudes **cvert,
		    DatabaseMsRad &db);
    
 public:

    MsRad();
    ~MsRad();
    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
    
};
#endif
